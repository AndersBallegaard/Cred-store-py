# Cred_store_py
### Cred_store_py is designed to be the least terrible way of storing passwords that need to be reversible.

## Why do you need reversible password storage?
Many people need to store passwords used by scripts. This could be an automation script that need to SSH into some servers or something that needs to store the password for an SQL server. there are many reasons why you need it but not many good solutions. But if you don't need to get the clear text password use something else.

## What is this project doing to solve the problem?
This project makes sure the password is only accessible on the computer it was created on and only if you know the username. besides this there is an option to set a secret string or require a specific user unlock the credentials this is optional ways to add a little more security add additional security.

## Features
This is the right project for you if you need encrypted reversible passwords made easy with support for multiple accounts and multiple levels of security levels even in the same credential store.