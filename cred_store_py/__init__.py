#imports
import getpass
import json
import base64
#from Crypto.Cipher import AES
#from Crypto import Random
from cryptography.fernet import Fernet
import os
import socket
import platform
import hashlib

#global variables
initiated = False
password_file = ""
system_key = ""
user = str(getpass.getuser())


#functions shown outside libary
__all__ = ["initiate","set_credentials","get_credentials","close"]

def read_store():
    """Read the password store and return it as normal json"""
    jf = open(password_file,"r+")
    jfd = {}
    try:
        jfd = json.loads(str(jf.read()))
    except:
        pass
    jf.close()
    return(dict(jfd))

def write_store(json_obj):
    """Write the json in json_obj to the datastore file"""
    jf = open(password_file,"w")
    data = json.dumps(json_obj)
    jf.write(data)
    jf.close()

def get_system_value():
    b64hn = str(socket.gethostname() + platform.system() + platform.processor()).encode("utf-8")
    s = hashlib.sha256()
    s.update(bytes(b64hn))
    return(str(s.digest()))

def check_initiated():
    """see if everything is initiated"""
    #checks if it's initiated and throws an error if not
    if not initiated:
        raise Exception("Please initiate before running this")


#encryption and decryption
#BS = 16
#pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
#unpad = lambda s : s[0:-ord(s[-1])]
#nonce = None
#def encrypt(key,raw):
#    raw = pad(raw)
#    iv = Random.new().read( AES.block_size )
#    cipher = AES.new( key, AES.MODE_CBC, iv )
#    return base64.b64encode( iv + cipher.encrypt( raw ) )

#def decrypt( key, enc ):
#    enc = base64.b64decode(enc)
#    print("enc:" + str(enc) + " len:" + str(len(enc)))
#    iv = enc[:16]
#    cipher = AES.new(key, AES.MODE_CBC, iv )
#    return unpad(cipher.decrypt( enc[16:] ))

def encrypt(key,raw):
    print(key)
    print(len(key))
    cipher = Fernet(key)
    return(cipher.encrypt(raw))

def decrypt(key,enc):
    cipher = Fernet(key)
    return(cipher.decrypt(enc))

def make_key_right(key):
    key1 = base64.b64encode(key.encode())
    if len(key1) < 32:
        key1 = 10 * key1
    return(base64.urlsafe_b64encode(hashlib.sha256(str(key1).encode("utf-8")).digest())[0:32])


def initiate(cred_store_file="credentials.json"):
    global initiated
    global system_key
    global password_file
    """Initiate the credential storage and encryption"""
    password_file = cred_store_file
    if not os.path.isfile(password_file):
         w = open(password_file,"w")
         w.close()
    system_key = get_system_value()
    initiated = True


def set_credentials(username,password,secret_string="",require_current_username=False):
    """Save a set of credentials to credential storage

options for using a secret string or requireing the current username to unlock the credentials
    """
    #check that everything is initiated
    check_initiated()

    uvar = ""
    if require_current_username:
        uvar = user

    encryption_key = str(secret_string) + str(uvar) + str(system_key)
    keychain = read_store()

    keychain[username] = str(base64.b64encode(encrypt(make_key_right(encryption_key),password)))

    write_store(keychain)

def get_credentials(username,secret_string="",require_current_username=False):
    """Get password based on username"""
    #check that everything is initiated
    check_initiated()

    uvar = ""
    if require_current_username:
        uvar = user

    encryption_key = str(system_key) + str(secret_string) + str(uvar)
    keychain = read_store()

    return(decrypt(make_key_right(encryption_key),keychain[username]))

def close():
    global password_file
    #check that everything is initiated
    check_initiated()
    password_file = ""