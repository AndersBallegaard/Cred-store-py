from setuptools import setup, find_packages

setup(
    name='Cred_store_py',
    author='Anders Ballegaard',
    author_email='anderstb[at]hotmail.dk',
    version='0.1',
    packages=find_packages(),
    license='MIT',
    long_description=open('readme.md').read(),
    install_requires=[
        "cryptography",
    ],
)
