#imports
import getpass
import json
import base64

#global variables
initiated = False
password_file = ""
system_key = ""
user = getpass.getuser()
jf = None

#functions shown outside libary
#__all__ = ["initiate","set_credentials","get_credentials","close"]

def read_store():
    """Read the password store and return it as normal json"""
    return(json.load(base64.b64decode(jf.read())))

def write_store(json_obj):
    """Write the json in json_obj to the datastore file"""
    jf.write(base64.b64encode(json_obj))

def check_initiated():
    """see if everything is initiated"""
    #checks if it's initiated and throws an error if not
    if not initiated:
        raise Exception("Please initiate before running this")
        


def initiate(cred_store_file=".credentials.json"):
    """Initiate the credential storage and encryption"""
    password_file = cred_store_file
    jf = open(password_file)
    initiate = True


def set_credentials(username,password,secret_string="",require_current_username=False):
    """Save a set of credentials to credential storage

options for using a secret string or requireing the current username to unlock the credentials
    """
    #check that everything is initiated
    check_initiated()

def get_credentials(username):
    """Get password based on username"""
    #check that everything is initiated
    check_initiated()

def close():
    #check that everything is initiated
    check_initiated()
    jf.close()
    jf = None
    password_file = ""