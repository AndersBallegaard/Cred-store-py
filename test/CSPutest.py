import cred_store_py
import unittest


class TestCredStore(unittest.TestCase):
    def test_normal_operation(self):
        cred_store_py.initiate()
        cred_store_py.set_credentials("user","pass")   
        self.assertEqual(cred_store_py.get_credentials("user"),"pass")

if __name__ == '__main__':
    unittest.main()